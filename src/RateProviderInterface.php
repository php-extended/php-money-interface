<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-money-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Money;

use DateTimeInterface;
use Stringable;

/**
 * RateProviderInterface interface file.
 *
 * This interface represents an object which can provides rate objects and
 * performs live conversions from currencies. The accuracy of the conversion
 * and the rates depends of the accuracy of the underlying library.
 *
 * @author Anastaszor
 */
interface RateProviderInterface extends Stringable
{
	
	/**
	 * Gets a rate value for given source currency, target currency and a valid
	 * datetime.
	 *
	 * @param CurrencyInterface $source
	 * @param CurrencyInterface $target
	 * @param DateTimeInterface $when
	 * @return ?RateInterface or null if cannot found and cannot be calculated
	 *                        or extrapolated
	 */
	public function getRateFromSourceToTargetAt(CurrencyInterface $source, CurrencyInterface $target, DateTimeInterface $when) : ?RateInterface;
	
	/**
	 * Converts the given amount of money from source currency to target
	 * currency at the time of given datetime.
	 *
	 * @param float $amount the amount in source currency
	 * @param CurrencyInterface $source
	 * @param CurrencyInterface $target
	 * @param DateTimeInterface $when
	 * @return ?float the converted amount to target currency, null if not found
	 *                and cannot be calculated or extrapolated
	 */
	public function convert(float $amount, CurrencyInterface $source, CurrencyInterface $target, DateTimeInterface $when) : ?float;
	
}
