<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-money-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Money;

use DateTimeInterface;
use Stringable;

/**
 * RateInterface interface file.
 *
 * This interface is a facade for all rate objects.
 *
 * @author Anastaszor
 */
interface RateInterface extends Stringable
{
	
	/**
	 * Gets the currency as source for the exchange rate value.
	 *
	 * @return CurrencyInterface
	 */
	public function getSourceCurrency() : CurrencyInterface;
	
	/**
	 * Gets the currency as target for the exchange rate value.
	 *
	 * @return CurrencyInterface
	 */
	public function getTargetCurrency() : CurrencyInterface;
	
	/**
	 * Gets the date and time when this rate value was valid.
	 *
	 * @return DateTimeInterface
	 */
	public function getDatetime() : DateTimeInterface;
	
	/**
	 * Gets the exact rate value. A value of 1 means that the source currency
	 * and the target currency have the same value.
	 *
	 * @return float
	 */
	public function getValue() : float;
	
}
