<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-money-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Money;

use Stringable;

/**
 * CurrencyInterface interface file.
 *
 * This interface is a facade for all currency objects.
 *
 * @author Anastaszor
 */
interface CurrencyInterface extends Stringable
{
	
	/**
	 * Gets the code iso 4217 of the currency code.
	 *
	 * @return string
	 */
	public function getCodeIso4217() : string;
	
	/**
	 * Gets the english name of the currency.
	 *
	 * @return string
	 */
	public function getNameEn() : string;
	
}
