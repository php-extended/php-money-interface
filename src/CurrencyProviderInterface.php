<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-money-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Money;

use InvalidArgumentException;
use Stringable;

/**
 * CurrencyProviderInterface interface file.
 *
 * This interface represents an object which can provides currency objects.
 *
 * @author Anastaszor
 */
interface CurrencyProviderInterface extends Stringable
{
	
	/**
	 * Gets a currency with the given code iso 4217 value.
	 *
	 * @param string $codeIso4217
	 * @return CurrencyInterface
	 * @throws InvalidArgumentException if the currency does not exists
	 */
	public function getCurrencyFromCodeIso4217(string $codeIso4217) : CurrencyInterface;
	
}
